last updated 2015.09.18
-------------------------------------------------------------------------------------------------------------
IP                   - location             - owner[1]    -    speed      - info/stats page[2]     - testnet
-------------------------------------------------------------------------------------------------------------
-
-------------------------------------------------------------------------------------------------------------
!!The following nodes have been unreachable at the time of the last update and might[3] be down!!
-------------------------------------------------------------------------------------------------------------
191.236.50.217   1   - Virgina, US          - grue        -    100 mbit/s - no                     - no
185.45.192.129   1   - Amsterdam, NL, EU    - anon        - 2x1000 mbit/s - node.cryptowatch.com   - yes
213.165.91.169   1   - Germany, EU          - shorena     -    100 mbit/s - yes                    - no
94.23.146.127    1   - NL, EU               - Mitchełł[5] -    200 mbit/s - /status                - no
-------------------------------------------------------------------------------------------------------------
[1] refers to a bitcointalk.org username or anon if requested
[2] same IP, port 80 or path/port given
[3] or reached max connections at the time of testing. Nodes failing 3 tests in a row will
be removed (Number of failed test is noted behind the IP)
[4] Runs BTCD (alternative full node implementation with GO)
[5] I dont want you search for those ł's on your keyboard, have a link
    https://bitcointalk.org/index.php?action=profile;u=113670